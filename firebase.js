const firebaseConfig = {
    apiKey: '',
    authDomain: 'kids-diary-0307.firebaseapp.com',
    databaseURL: 'https://kids-diary-0307.firebaseio.com',
    projectId: 'kids-diary-0307',
    storageBucket: '',
    messagingSenderId: ''
};

let firebase = require('firebase');

const firebaseApp = firebase.initializeApp(firebaseConfig);
const rootRef = firebaseApp.database().ref();

function getData(child) {			//child: 位置
	return rootRef.child(child).once('value').then((json) => {
		return json;
	});
}

function pushData(child, data) {	//child: 位置，data: JSON資料
	return rootRef.child(child).update(data);
}

exports.getData = getData;
exports.pushData = pushData;
exports.rootRef = rootRef;
