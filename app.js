let key = '';
let groupName = 'kids_diary';

let express = require('express');
let bodyParser = require('body-parser');
let request = require('request');
let { getData, pushData, rootRef } = require('./firebase.js');

let app = express();

app.use(bodyParser.json());

app.post('/addFace', function (req, res) {//更新單一相片人臉
  res.send('data');

	let data = req.body;
	//data: {user: 'user', uri: '網址'}

  console.log('');
  console.log('POST addFace To ' + data.user + '		uri：' + data.uri);
  addFace(data.user, data.uri);
});

app.post('/addPerson', function (req, res) {//更新單一相片人臉
  res.send('data');

	let data = req.body;
	//data: {user: 'user'}

  console.log('');
  console.log('POST addPerson：' + data.user);
  addPerson(data.user);
});

app.get('/trainPerson', function (req, res) {//培訓人員組
  res.send('data');

  console.log('');
  console.log('GET trainPerson');
  train();
});

app.post('/updateFace', function (req, res) {//更新單一相片人臉
  res.send('data');

	let data = req.body;
	//data: {uri: '網址'}

  console.log('');
  console.log('POST updateFace uri：' + data.uri);
  updateFace(data.uri);
});

app.post('/updatePhoto', function (req, res) {//更新相片
  res.send('data');

	let data = req.body;
	//data: {classname: tiger}

  console.log('');
  console.log('POST updatePhoto classname：' + data.classname);
  updatePhoto(data.classname);
});


function addFace(user, faceUri) {

	getData('faceData').then(json => {
		let data = json.val();
		let personId;
		for(let i=0 ; i<data.length ; i++){
			if(data[i].name == user){
				personId = data[i].personId;
				break;
			}
		}

		setTimeout(() => {
			let uri = 'https://southeastasia.api.cognitive.microsoft.com/face/v1.0/persongroups/' + groupName + '/persons/' + personId + '/persistedFaces';

			request({
				url: uri,
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Ocp-Apim-Subscription-Key': key
				},
				json: {
					'url': faceUri
				}
			}, (err, res, body) => {
				if(res.statusCode == 200){
  				console.log('add one face to ' + user);
  				console.log(body);
  				train();
				}
			});
		}, 1000);
	})
}

function addPerson(user) {
	let uri = 'https://southeastasia.api.cognitive.microsoft.com/face/v1.0/persongroups/' + groupName + '/persons';

	request({
		url: uri,
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Ocp-Apim-Subscription-Key': key
		},
		json: {
			'name': user
		}
	}, (err, res, body) => {
		if(res.statusCode == 200){
			getData('faceData').then(json => {
				let data = json.val();
				data.push({name: user, personId: body.personId});

				pushData('faceData', data);
				console.log('addPerson ' + user + ' Success!!');
			});
		}
	});
}

function train() {//培訓人員組
	let uri = 'https://southeastasia.api.cognitive.microsoft.com/face/v1.0/persongroups/' + groupName + '/train';

	request({
		url: uri,
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Ocp-Apim-Subscription-Key': key
		}
	}, (err, res, body) => {
		if(res.statusCode == 202){
			uri = 'https://southeastasia.api.cognitive.microsoft.com/face/v1.0/persongroups/' + groupName + '/training';

			request({
				url: uri,
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					'Ocp-Apim-Subscription-Key': key
				}
			}, (err, res, body) => {
				if(res.statusCode == 200){
					console.log(body);
				}
			});
		}
	});
}

function updateFace(uri) {	//更新人臉1
  getData('ip').then((json) => {
  	let server = json.val();

  	getData('faceData').then((json) => {
  		let faceList = json.val();

			judge(server, faceList, uri);
  	});
  });
}

function updatePhoto(classname) {//更新相片1
  getData('ip').then((json) => {
  	let server = json.val();

  	getData('faceData').then((json) => {
  		let faceList = json.val();

			for(let i=0 ; i<faceList.length ; i++){
				rootRef.child('users/' + faceList[i].name + '/album').set(['']);
			}

			setTimeout(function(){
			  getData('class/' + classname + '/album').then((json) => {
					let database = json.val();

					for(let i=0 ; i<database.length ; i++){
						for(let j=0 ; j<database[i].photos.length ; j++){

							setTimeout(function(){
								judge(server, faceList, server+database[i].photos[j]);
							}, 5000*(i+1)*(j+1));
						}
					}
			  });
			}, 10000);
  	});
  });
}

function judge(server, faceList, photoURI) {//更新相片2, 更新人臉2
	console.log(photoURI);
	let uri = 'https://southeastasia.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceId=true&returnFaceLandmarks=false';

	request({
		url: uri,
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Ocp-Apim-Subscription-Key': key
		},
		json: {
			'url': photoURI
		}
	}, (err, res, body) => {
		console.log('detect people：'+body.length);
		if(res.statusCode == 200){
			uri = 'https://southeastasia.api.cognitive.microsoft.com/face/v1.0/identify';
			let face = [], faceTemp = [];

			for(var i=0; i< body.length; i++){
				face.push(body[i].faceId);
			}

			
			while(face.length != 0){
				if(face.length > 10){
					faceTemp = [];
					for(let i=0 ; i< 10 ; i++){
						faceTemp.push(face.shift());
					}
				}else{
					faceTemp = face;
					face = [];
				}

				request({
					url: uri,
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						"Ocp-Apim-Subscription-Key": key
					},
					json: {
				    "faceIds": faceTemp,
				    'maxNumOfCandidatesReturned':1,
				    "personGroupId": groupName,
				    'confidenceThreshold': 0.5
					}
				}, (err, res, body) => {
					console.log('identify people：'+body.length);
					if(res.statusCode == 200){
						for(let i=0 ; i<body.length ; i++){
							if(body[i].candidates.length != 0){
								for(let j=0 ; j<faceList.length ; j++){
									if(faceList[j].personId == body[i].candidates[0].personId){
										console.log(faceList[j].name+'：'+photoURI);
										
										getData('users/' + faceList[j].name + '/album').then((json) => {
											let jsonData = json.val();
											if(jsonData[0] == ''){
												jsonData[0] = photoURI.substring(server.length);
											}else{
												jsonData.push(photoURI.substring(server.length));
											}
											rootRef.child('users/' + faceList[j].name + '/album').set(jsonData);
										});
									}
								}
							}
						}
					}else{
						console.log(err);
					}
				});
			}
		}else{
			console.log(err);
		}
	});
}

app.listen(3000);